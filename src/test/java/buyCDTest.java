import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class buyCDTest {
    @Test
    @DisplayName("Buy a cd when in stock")
    public void buyACDWhenInStock() {
        CompactDisc cd = new CompactDisc(10);
        cd.buy(1);
        assertEquals(9, cd.getStockCount());
    }
}

//